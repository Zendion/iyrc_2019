-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: iyrc_2019
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.38-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alunos`
--

DROP TABLE IF EXISTS `alunos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alunos` (
  `idAlunos` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(150) DEFAULT NULL,
  `idEscolas` int(11) DEFAULT NULL,
  `idSeries` int(11) DEFAULT NULL,
  `Participou` tinyint(4) DEFAULT '0',
  `Participacoes` int(11) DEFAULT '0',
  PRIMARY KEY (`idAlunos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alunos`
--

LOCK TABLES `alunos` WRITE;
/*!40000 ALTER TABLE `alunos` DISABLE KEYS */;
/*!40000 ALTER TABLE `alunos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escolas`
--

DROP TABLE IF EXISTS `escolas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `escolas` (
  `idEscolas` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`idEscolas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escolas`
--

LOCK TABLES `escolas` WRITE;
/*!40000 ALTER TABLE `escolas` DISABLE KEYS */;
/*!40000 ALTER TABLE `escolas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `participacao`
--

DROP TABLE IF EXISTS `participacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participacao` (
  `idParticipacao` int(11) NOT NULL AUTO_INCREMENT,
  `idAluno1` int(11) DEFAULT NULL,
  `idAluno2` int(11) DEFAULT NULL,
  `idProvas` int(11) DEFAULT NULL,
  `P1` double DEFAULT NULL,
  `P2` double DEFAULT NULL,
  `P3` double DEFAULT NULL,
  `Tempo` time DEFAULT NULL,
  PRIMARY KEY (`idParticipacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `participacao`
--

LOCK TABLES `participacao` WRITE;
/*!40000 ALTER TABLE `participacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `participacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provas`
--

DROP TABLE IF EXISTS `provas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provas` (
  `idProvas` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) DEFAULT NULL,
  `Desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idProvas`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provas`
--

LOCK TABLES `provas` WRITE;
/*!40000 ALTER TABLE `provas` DISABLE KEYS */;
INSERT INTO `provas` VALUES (1,'Basquete',NULL),(2,'Futebol',NULL),(3,'Humanoide',NULL),(4,'Trajeto de obstáculos',NULL),(5,'Volei',NULL),(6,'Save the Forest',NULL),(7,'Line Follower',NULL);
/*!40000 ALTER TABLE `provas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `series`
--

DROP TABLE IF EXISTS `series`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `series` (
  `idSeries` int(11) NOT NULL AUTO_INCREMENT,
  `Desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idSeries`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `series`
--

LOCK TABLES `series` WRITE;
/*!40000 ALTER TABLE `series` DISABLE KEYS */;
INSERT INTO `series` VALUES (0,'*'),(1,'1º A'),(2,'1º B'),(3,'1º C'),(4,'1º D'),(5,'1º E'),(6,'1º F'),(7,'2º A'),(8,'2º B'),(9,'2º C'),(10,'2º D'),(11,'2º E'),(12,'2º F'),(13,'3º A'),(14,'3º B'),(15,'3º C'),(16,'3º D'),(17,'3º E'),(18,'3º F'),(19,'4º A'),(20,'4º B'),(21,'4º C'),(22,'4º D'),(23,'4º E'),(24,'4º F'),(25,'5º A'),(26,'5º B'),(27,'5º C'),(28,'5º D'),(29,'5º E'),(30,'5º F'),(31,'6º A'),(32,'6º B'),(33,'6º C'),(34,'6º D'),(35,'6º E'),(36,'6º F'),(37,'7º A'),(38,'7º B'),(39,'7º C'),(40,'7º D'),(41,'7º E'),(42,'7º F'),(43,'8º A'),(44,'8º B'),(45,'8º C'),(46,'8º D'),(47,'8º E'),(48,'8º F'),(49,'9º A'),(50,'9º B'),(51,'9º C'),(52,'9º D'),(53,'9º E'),(54,'9º F'),(55,'1º EM A'),(56,'1º EM B'),(57,'1º EM C'),(58,'1º EM D'),(59,'1º EM E'),(60,'1º EM F'),(61,'2º EM A'),(62,'2º EM B'),(63,'2º EM C'),(64,'2º EM D'),(65,'2º EM E'),(66,'2º EM F'),(67,'3º EM A'),(68,'3º EM B'),(69,'3º EM C'),(70,'3º EM D'),(71,'3º EM E'),(72,'3º EM F'),(73,'MATERNAL 2M '),(74,'MATERNAL 2 T'),(75,'JARDIM 1M'),(76,'JARDIM 1T'),(77,'JARDIM 2M'),(78,'JARDIM 2T'),(79,'1° ANO M'),(80,'1° ANO T'),(81,'2° ANO M'),(82,'2° ANO T'),(83,'3°ANO M '),(84,'3°ANO T'),(85,'4° ANO M '),(86,'4° ANO T'),(87,'5° ANO M'),(88,'5° ANO T'),(89,'6° ANO M '),(90,'6° ANO T'),(91,'7° ANO M'),(92,'8° ANO M '),(93,'8° ANO T'),(94,'9° ANO M '),(95,'9° ANO T'),(96,'1º EM'),(97,'2º EM'),(98,'9º ANO'),(99,'7º ANO'),(100,'8º ANO'),(101,'6º ANO');
/*!40000 ALTER TABLE `series` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-13 15:00:10
