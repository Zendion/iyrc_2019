const express = require('express');
const router = express.Router();
const formidable = require('formidable');

var routes = function () {

  // ALUNOS
  router.get('/getAllAlunos', function (req, res, next) {
    const sqlQuery = `SELECT * FROM Alunos;`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/getAllAlunos error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/getAllAlunos success 200");
        }
    });
  });

  router.get('/getListAlunos', function (req, res, next) {
    const sqlQuery = `
SELECT 
	idAlunos,
    Nome,
    idEscolas,
    (SELECT es.Nome FROM escolas es WHERE idEscolas = al.idEscolas) AS 'Escola',
    idSeries,
    (SELECT se.Desc FROM series se WHERE idSeries = al.idSeries) AS 'Ano',
    CASE WHEN Participou = 0 THEN 'Não' ELSE 'Sim' END AS 'Participou'
FROM 
	Alunos al;`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/getListAlunos error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/getListAlunos success 200");
        }
    });
  });

  router.get('/getAlunoByID/:idAlunos', function (req, res, next) {
    let idAlunos = req.params.idAlunos;
    const sqlQuery = `SELECT * FROM Alunos WHERE idAlunos = ${idAlunos};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/getAlunoByID error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/getAlunoByID success 200");
        }
    });
  });

  router.post('/insertAlunos', function (req, res, next) {
    let idAlunos = req.body.idAlunos;
    let Nome = req.body.Nome;
    let idEscolas = req.body.idEscolas;
    let idSeries = req.body.idSeries;

    const sqlQuery = `INSERT INTO Alunos (Nome, idEscolas, idSeries) VALUES ('${Nome}', ${idEscolas}, ${idSeries});`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/insertAlunos error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/insertAlunos success 200");
        }
    });
  });

  router.patch('/updateAlunos', function (req, res, next) {
    let idAlunos = req.body.idAlunos;
    let Nome = req.body.Nome;
    let idEscolas = req.body.idEscolas;
    let idSeries = req.body.idSeries;
    let Participou = req.body.Participou ? 1 : 0;
    let Participacoes = req.body.Participacoes;
    
    const sqlQuery = `
    UPDATE
        Alunos
    SET
        Nome = '${Nome}',
        idEscolas = ${idEscolas},
        idSeries = ${idSeries},
        Participou = ${Participou},
        Participacoes = ${Participacoes}
    WHERE
        idAlunos = ${idAlunos};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/updateAlunos error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/updateAlunos success 200");
        }
    });
  });

  router.delete('/deleteAlunos/:idAlunos', function (req, res, next) {
    let idAlunos = req.params.idAlunos;

    const sqlQuery = `
    DELETE FROM
        Alunos
    WHERE
        idAlunos = ${idAlunos};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/deleteAlunos error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/deleteAlunos success 200");
        }
    });
  });

  return router;
}

module.exports = routes;