const express = require('express');
const router = express.Router();
const formidable = require('formidable');

var routes = function () {

  // ESCOLAS
  router.get('/getAllEscolas', function (req, res, next) {
    const sqlQuery = `SELECT * FROM Escolas;`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/escolas/getAllEscolas error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/escolas/getAllEscolas success 200");
        }
    });
  });

  router.get('/getEscolaByID/:idEscolas', function (req, res, next) {
    let idEscolas = req.params.idEscolas;
    const sqlQuery = `SELECT * FROM Escolas WHERE idEscolas = ${idEscolas};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/escolas/getEscolaByID error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/escolas/getEscolaByID success 200");
        }
    });
  });

  router.post('/insertEscolas', function (req, res, next) {
    let idEscolas = req.body.idEscolas;
    let Nome = req.body.Nome;

    const sqlQuery = `INSERT INTO Escolas (Nome) VALUES ('${Nome}');`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/escolas/insertEscolas error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/escolas/insertEscolas success 200");
        }
    });
  });

  router.patch('/updateEscolas', function (req, res, next) {
    let idEscolas = req.body.idEscolas;
    let Nome = req.body.Nome;
    
    const sqlQuery = `
    UPDATE
        Escolas
    SET
        Nome = '${Nome}'        
    WHERE
        idEscolas = ${idEscolas};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/escolas/updateEscolas error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/escolas/updateEscolas success 200");
        }
    });
  });

  router.delete('/deleteEscolas/:idEscolas', function (req, res, next) {
    let idEscolas = req.params.idEscolas;

    const sqlQuery = `
    DELETE FROM
        Escolas
    WHERE
        idEscolas = ${idEscolas};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/escolas/deleteEscolas error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/escolas/deleteEscolas success 200");
        }
    });
  });

  return router;
}

module.exports = routes;