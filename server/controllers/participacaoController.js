const express = require('express');
const router = express.Router();
const formidable = require('formidable');

var routes = function () {

  // PARTICIPACAO
  router.get('/getAllParticipacao', function (req, res, next) {
    const sqlQuery = `SELECT * FROM participacao;`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/participacao/getAllParticipacao error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/participacao/getAllParticipacao success 200");
        }
    });
  });

  router.get('/getParticipacaoByID/:idParticipacao', function (req, res, next) {
    let idParticipacao = req.params.idParticipacao;
    const sqlQuery = `SELECT * FROM Participacao WHERE idParticipacao = ${idParticipacao};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/participacao/getParticipacaoByID error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/participacao/getParticipacaoByID success 200");
        }
    });
  });

  router.get('/getParticipacoesByAluno/:idAlunos', function (req, res, next) {
    let idAlunos = req.params.idAlunos;
    const sqlQuery = `SELECT DISTINCT idProvas, idAluno1 FROM Participacao WHERE idAluno1 = ${idAlunos} OR idAluno2 = ${idAlunos};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/participacao/getParticipacoesByAluno error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/participacao/getParticipacoesByAluno success 200");
        }
    });
  });

  router.post('/insertParticipacao', function (req, res, next) {
    let idParticipacao = req.body.idParticipacao;
    let idAluno1 = req.body.idAluno1;
    let idAluno2 = req.body.idAluno2;
    let idProvas = req.body.idProvas;
    let P1 = req.body.P1;
    let P2 = req.body.P2;
    let P3 = req.body.P3;
    let Tempo = req.body.Tempo;

    const sqlQuery = `INSERT INTO Participacao (idAluno1, idAluno2, idProvas, P1, P2, P3, Tempo) VALUES (${idAluno1}, ${idAluno2}, ${idProvas}, ${P1}, ${P2}, ${P3}, '${Tempo}');`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/participacao/insertParticipacao error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/participacao/insertParticipacao success 200");
        }
    });
  });

  router.patch('/updateParticipacao', function (req, res, next) {
    let idParticipacao = req.body.idParticipacao;
    let idAluno1 = req.body.idAluno1;
    let idAluno2 = req.body.idAluno2;
    let idProvas = req.body.idProvas;
    let P1 = req.body.P1;
    let P2 = req.body.P2;
    let P3 = req.body.P3;
    let Tempo = req.body.Tempo;
    
    const sqlQuery = `
    UPDATE
        Participacao
    SET
        idAluno1 = ${idAluno1},
        idAluno2 = ${idAluno2},
        idProvas = ${idProvas},
        P1 = ${P1},
        P2 = ${P2},
        P3 = ${P3},
        Tempo = '${Tempo}'
    WHERE
        idParticipacao = ${idParticipacao};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/participacao/updateParticipacao error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/participacao/updateParticipacao success 200");
        }
    });
  });

  router.delete('/deleteParticipacao/:idParticipacao', function (req, res, next) {
    let idParticipacao = req.params.idParticipacao;

    const sqlQuery = `
    DELETE FROM
        Participacao
    WHERE
        idParticipacao = ${idParticipacao};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/participacao/deleteParticipacao error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/participacao/deleteParticipacao success 200");
        }
    });
  });

  router.get('/getPlacar/:idProvas', function (req, res, next) {
    let idProvas = req.params.idProvas;
    const sqlQuery = `
SELECT 
	b.Prova,
    b.Alunos,
    b.Escola,
    b.Ano
FROM
	(SELECT 
		a.Prova,
		a.Alunos,
		a.Escola,
		a.Ano,
		SUM(a.Score) AS Score,
        a.Tempo
	FROM
		(SELECT
			pr.Nome AS 'Prova',
			CONCAT(
				CASE WHEN (SELECT Nome FROM alunos WHERE idAlunos = pa.idAluno2) IS NULL THEN (SELECT Nome FROM alunos WHERE idAlunos = pa.idAluno1) ELSE SUBSTRING_INDEX((SELECT Nome FROM alunos WHERE idAlunos = pa.idAluno1), ' ', 1) END, 
				CASE WHEN (SELECT Nome FROM alunos WHERE idAlunos = pa.idAluno2) IS NULL THEN '' ELSE ' e ' END, 
				COALESCE(SUBSTRING_INDEX((SELECT Nome FROM alunos WHERE idAlunos = pa.idAluno2), ' ', 1), '')
			) AS 'Alunos',
			CASE WHEN (SELECT Nome FROM alunos WHERE idAlunos = pa.idAluno2) IS NULL OR (SELECT idEscolas FROM alunos WHERE idAlunos = pa.idAluno1) = (SELECT idEscolas FROM alunos WHERE idAlunos = pa.idAluno2) THEN (SELECT Nome FROM escolas WHERE idEscolas = al.idEscolas) ELSE CONCAT((SELECT Nome FROM escolas WHERE idEscolas = (SELECT idEscolas FROM alunos WHERE idAlunos = pa.idAluno1)), ' e ', (SELECT Nome FROM escolas WHERE idEscolas = (SELECT idEscolas FROM alunos WHERE idAlunos = pa.idAluno2))) END AS 'Escola',
			CASE WHEN (SELECT Nome FROM alunos WHERE idAlunos = pa.idAluno2) IS NULL OR (SELECT idSeries FROM alunos WHERE idAlunos = pa.idAluno1) = (SELECT idSeries FROM alunos WHERE idAlunos = pa.idAluno2) THEN (SELECT series.Desc FROM series WHERE idSeries = al.idSeries) ELSE CONCAT((SELECT series.Desc FROM series WHERE idSeries = (SELECT idSeries FROM alunos WHERE idAlunos = pa.idAluno1)), ' e ', (SELECT series.Desc FROM series WHERE idSeries = (SELECT idSeries FROM alunos WHERE idAlunos = pa.idAluno2))) END AS 'Ano',
			(pa.P1 + pa.P2 + pa.P3) AS 'Score',
            pa.Tempo as 'Tempo'
		FROM
			participacao pa
		INNER JOIN provas pr ON pa.idProvas = pr.idProvas
		INNER JOIN alunos al ON al.idAlunos = pa.idAluno1
		INNER JOIN escolas es ON es.idEscolas = al.idEscolas
		INNER JOIN series se ON se.idSeries = al.idSeries
		WHERE 
			pr.idProvas = ${idProvas}) a GROUP BY a.Alunos) b
	ORDER BY b.Score DESC, b.Tempo ASC`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/participacao/getPlacar error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/participacao/getPlacar success 200");
        }
    });
  });

  return router;
}

module.exports = routes;