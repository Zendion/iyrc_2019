const express = require('express');
const router = express.Router();

var routes = function () {

  // SERIES
  router.get('/getAllProvas', function (req, res, next) {
    const sqlQuery = `SELECT * FROM Provas;`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/getAllProvas error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/getAllProvas success 200");
        }
    });
  });

  router.get('/getProvaByID/:idProvas', function (req, res, next) {
    let idProvas = req.params.idProvas;
    const sqlQuery = `SELECT * FROM Provas WHERE idProvas = ${idProvas};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/getProvaByID error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/getProvaByID success 200");
        }
    });
  });

  return router;
}

module.exports = routes;