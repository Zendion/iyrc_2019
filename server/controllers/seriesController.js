const express = require('express');
const router = express.Router();

var routes = function () {

  // SERIES
  router.get('/getAllSeries', function (req, res, next) {
    const sqlQuery = `SELECT * FROM Series;`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/getAllSeries error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/getAllSeries success 200");
        }
    });
  });

  router.get('/getSerieByID/:idSeries', function (req, res, next) {
    let idSeries = req.params.idSeries;
    const sqlQuery = `SELECT * FROM Series WHERE idSeries = ${idSeries};`;
    connection.query(sqlQuery, function (error, results, fields) {
        if (error) {
            res.send(JSON.stringify({ "status": 500, "error": error, "response": null }))
            console.log("/alunos/getSerieByID error 500");
        }
        else {
            res.send(JSON.stringify({ "status": 200, "error": null, "response": results }))
            console.log("/alunos/getSerieByID success 200");
        }
    });
  });

  return router;
}

module.exports = routes;