const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mysql = require('mysql');

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200
}

app.use(cors(corsOptions));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.listen(1337, '0.0.0.0', () => {
    console.log('IYRC Server iniciado');
});

app.use(function (req, res, next) {
    global.connection = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'IYRC_2019',
    });
    connection.connect();
    next();
    connection.end();
});

app.use('/escolas', require('./controllers/escolasController')());
app.use('/alunos', require('./controllers/alunosController')());
app.use('/series', require('./controllers/seriesController')());
app.use('/provas', require('./controllers/provasController')());
app.use('/participacao', require('./controllers/participacaoController')());