import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { JogosComponent } from './pages/jogos/jogos.component';
import { FutebolComponent } from './pages/jogos/futebol/futebol.component';
import { BasqueteComponent } from './pages/jogos/basquete/basquete.component';
import { VoleiComponent } from './pages/jogos/volei/volei.component';
import { HumanoideComponent } from './pages/jogos/humanoide/humanoide.component';
import { MainMenuComponent} from './pages/main-menu/main-menu.component';
import { LoginComponent } from './pages/login/login.component';
import { TrajetoComponent } from './pages/jogos/trajeto/trajeto.component';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { PlacarComponent } from './pages/placar/placar.component';
import { FlorestaComponent } from './pages/jogos/floresta/floresta.component';
import { SeguidorComponent } from './pages/jogos/seguidor/seguidor.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { 
    path: 'login', 
    component : LoginComponent
  },{
    path: 'mainmenu',
    component: MainMenuComponent
  },{
    path: 'cadastro',
    component: CadastroComponent,
    children: [
      {
        path:'alunos',
        component: CadastroComponent
      },{
        path:'escolas',
        component: CadastroComponent
      }
    ]
  },{
    path: 'jogos',
    component: JogosComponent,
  },{
    path: 'placar',
    component: PlacarComponent,
  },{
    path: 'jogos/futebol',
    component: FutebolComponent,
  },{
    path: 'jogos/basquete',
    component: BasqueteComponent,
  },{
    path: 'jogos/volei',
    component: VoleiComponent,
  },{
    path: 'jogos/humanoide',
    component: HumanoideComponent,
  },{
    path: 'jogos/trajeto',
    component: TrajetoComponent,
  },{
    path: 'jogos/floresta',
    component: FlorestaComponent,
  },{
    path: 'jogos/seguidor',
    component: SeguidorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
