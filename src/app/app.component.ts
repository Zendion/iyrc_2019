import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'IYRC2019System';

  ngOnInit(): void {
    let req = new XMLHttpRequest();
    let formData = new FormData();
    req.open("GET", "assets/IP.txt");
    req.send(formData);
    req.onreadystatechange = function() {
      if (req.readyState === 4) {
        localStorage.setItem("IP", req.response);
      }
    }
  }

}
