import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { JogosComponent } from './pages/jogos/jogos.component';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FutebolComponent } from './pages/jogos/futebol/futebol.component';
import { BasqueteComponent } from './pages/jogos/basquete/basquete.component';
import { VoleiComponent } from './pages/jogos/volei/volei.component';
import { HumanoideComponent } from './pages/jogos/humanoide/humanoide.component';
import { AlunoService } from './services/alunoService';
import { EscolaService } from './services/escolaSerivce';
import { ParticipacaoService } from './services/participacaoService';
import { ProvasService } from './services/provasService';
import { SerieService } from './services/serieSerivice';
import { TrajetoComponent } from './pages/jogos/trajeto/trajeto.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { MainMenuComponent } from './pages/main-menu/main-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { PlacarComponent } from './pages/placar/placar.component';
import { CompetidorComponent } from './pages/placar/competidor/competidor.component';
import { FlorestaComponent } from './pages/jogos/floresta/floresta.component';
import { SeguidorComponent } from './pages/jogos/seguidor/seguidor.component';
import { GlobalFunctions } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    JogosComponent,
    FutebolComponent,
    BasqueteComponent,
    VoleiComponent,
    HumanoideComponent,
    TrajetoComponent,
    MainMenuComponent,
    CadastroComponent,
    PlacarComponent,
    CompetidorComponent,
    FlorestaComponent,
    SeguidorComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    TooltipModule.forRoot(),
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [
    AlunoService,
    EscolaService,
    ParticipacaoService,
    ProvasService,
    SerieService,
    GlobalFunctions
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
