export class Alunos {
    idAlunos: number;
    Nome: string;
    idEscolas: number;
    idSeries: number;
    Participou: Boolean;
    Participacoes: number;
}

export class ListAlunos {
    idAlunos: number;
    Nome: string;
    idEscolas: number;
    Escola: string;
    idSeries: number;
    Ano: string;
    Participou: string;
}