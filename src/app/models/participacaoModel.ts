export class Participacao {
    idParticipacao: number;
    idAluno1: number;
    idAluno2: number;
    idProvas: number;
    P1: number;
    P2: number;
    P3: number;
    Tempo: string;
}

export class ParticipacaoCont {
    idAluno1: number;
    idProvas: number;
}