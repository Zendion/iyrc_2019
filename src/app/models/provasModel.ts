export class Provas {
    idProvas: number;
    Nome: string;
    Desc: string;
}

export class Placar {
    Prova: string;
    Aluno: string;
    Escola: string;
    Ano: string;
}