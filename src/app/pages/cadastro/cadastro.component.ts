import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { NgModel } from '@angular/forms';
import { Escolas } from 'src/app/models/escolasModel';
import { EscolaService } from 'src/app/services/escolaSerivce';
import { ToastrService } from 'ngx-toastr';
import { Series } from 'src/app/models/seriesModel';
import { SerieService } from 'src/app/services/serieSerivice';
import { Alunos, ListAlunos } from 'src/app/models/alunosModel';
import { AlunoService } from 'src/app/services/alunoService';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.scss']
})
export class CadastroComponent implements OnInit {
  place = "";
  aluno = false;
  series = Array<Series>();
  escolas = Array<Escolas>();
  alunos = Array<ListAlunos>();
  regAluno = new Alunos();
  escola = new Escolas();
  Cadastrado: Boolean = false;
  AlunoCadastrado = new ListAlunos();
  constructor(private router: Router,
              private escolaServie: EscolaService,
              private seriesService: SerieService,
              private alunoService: AlunoService,
              private toastr: ToastrService) { }

  ngOnInit() {
    if (this.router.url === "/cadastro/alunos")
    {
      this.place = "Aluno";
      this.aluno = true;
      this.regAluno.idEscolas = 0;
      this.regAluno.idSeries = 0;
    }
    else
    {
      this.place = "Escola";
    }
    this.seriesService.getAllSeries().then(resSeries => {
      this.series = resSeries;
    })
    this.escolaServie.getAllEscolas().then(resEscolas => {
      this.escolas = resEscolas;
    })
    this.alunoService.getListAlunos().then(resAlunos => {
      this.alunos = resAlunos;
    })
  }

  onSubmit()
  {
    if (!this.aluno)
    {
      if (this.escola.Nome == "" || this.escola.Nome == null || this.escola.Nome == undefined) {
        this.toastr.error("Preencha corretamente o nome da escola");
        return;
      }
      this.escolaServie.insertEscolas(this.escola).then(response => {
        if (response['status'] == 200)
        {
          this.toastr.success("Cadastro efetuado com sucesso!");
          setTimeout(() => {
            this.Back();
          }, 1000);
        }
        else
        {
          this.toastr.error("Erro no cadastro, verifique sua conexão ou contate Ricardo");
        }
      });
    }
    else
    {
      if (this.regAluno.Nome == "" || this.regAluno.Nome == null || this.regAluno.Nome == undefined) {
        this.toastr.error("Preencha corretamente o nome do aluno");
        return;
      }
      this.alunoService.insertAlunos(this.regAluno).then(response => {
        if (response['status'] == 200)
        {
          document.getElementById('btnCadastrar').setAttribute('disabled', 'true');
          document.getElementById('divContent').setAttribute('hidden', 'true'); 
          this.alunoService.getListAlunos().then(resLista => {
            for (let i = 0; i < resLista.length; i++) {
              if (response['response'].insertId == resLista[i].idAlunos) {
                this.AlunoCadastrado = resLista[i];
                this.Cadastrado = true;
                this.toastr.success("Cadastro efetuado com sucesso!");
              }
            }
          })
        }
        else
        {
          this.toastr.error("Erro no cadastro, verifique sua conexão ou contate Ricardo");
        }
      });
    }

  }

  Back() {
    this.router.navigate(['mainmenu']);
  }

}
