import { Component, OnInit } from '@angular/core';
import { Participacao } from 'src/app/models/participacaoModel';
import { Alunos } from 'src/app/models/alunosModel';
import { ParticipacaoService } from 'src/app/services/participacaoService';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AlunoService } from 'src/app/services/alunoService';

@Component({
  selector: 'app-floresta',
  templateUrl: './floresta.component.html',
  styleUrls: ['./floresta.component.scss']
})
export class FlorestaComponent implements OnInit {

  alunoChosen: Boolean = false;

  Participacao: Participacao;
  Lista: Alunos[];
  Aluno: Alunos;
  btnText: string = "Verificar";
  Verificado: Boolean = false;
  DesabilitaBotao: Boolean = false;

  constructor(
    private participacaoService: ParticipacaoService,
    private toastr: ToastrService,
    private route: Router,
    private alunosService: AlunoService,
  ) { }

  ngOnInit() {
    this.DesabilitaBotao = false;
    this.Participacao = new Participacao();
    this.Participacao.idProvas = 6;
    this.Participacao.idAluno2 = null;
    this.Participacao.P1 = 0;
    this.Participacao.P2 = 0;
    this.Participacao.P3 = 0;
    this.Participacao.Tempo = "03:00:00";

    this.alunosService.getAllAlunos().then(resLista => {
      this.Lista = resLista;
    });
  }

  ChangeNbr(Cesta: number, addNbr: number) {
    switch (Cesta) {
      case 1:
        if (addNbr == -1) {
          if (this.Participacao.P1 != 0)
            this.Participacao.P1 += addNbr;
        }
        else {
          if (this.Participacao.P1 != 3)
            this.Participacao.P1 += addNbr;
        }
      break;
      case 2:
        if (addNbr == -1) {
          if (this.Participacao.P2 != 0)
            this.Participacao.P2 += addNbr;
        }
        else {
          if (this.Participacao.P2 != 3)
            this.Participacao.P2 += addNbr;
        }
      break;
    }
  }

  ConfereAluno() {
    if (this.btnText == "Verificar") {
      let Encontrado: boolean = false;
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.Participacao.idAluno1 == this.Lista[i].idAlunos) {
          Encontrado = true;
          this.Verificado = true;
          this.Aluno = this.Lista[i];
          this.participacaoService.getParticipacoesByAluno(this.Participacao.idAluno1).then(resQtde => {
            if (resQtde.length < 2) {
              for (let i = 0; i < resQtde.length; i++) {
                if (resQtde[i]['idProvas'] == this.Participacao.idProvas && this.Aluno.Participacoes > 2) {
                  this.toastr.error('Participante já participou dessa prova!');
                  return;
                }
              }
              this.Aluno.Participou = false;
              this.btnText = "Começar Prova";
            }
            else if (resQtde.length == 2 && (this.Participacao.idProvas == resQtde[0]['idProvas'] || this.Participacao.idProvas == resQtde[1]['idProvas'])) {
              for (let i = 0; i < resQtde.length; i++) {
                if (resQtde[i]['idProvas'] == this.Participacao.idProvas && this.Aluno.Participacoes < 3) {
                  this.Aluno.Participou = false;
                  this.btnText = "Começar Prova";
                  return;
                }
              }
              this.Aluno.Participou = true;
              this.toastr.error('Número do participante inválido ou já participou de outra prova!');
            }
            else {
              this.Aluno.Participou = true;
              this.toastr.error('Número do participante inválido ou já participou de outra prova!');
            }
          });
        }
      }
      if (!Encontrado) {
        this.toastr.error('Número do participante não encontrado!');
        this.Verificado = false;
      }
    }
    else {
      this.alunoChosen = true;
    }
  }

  End() {
    this.DesabilitaBotao = true;
    this.participacaoService.insertParticipacao(this.Participacao).then(resParticipacao => {
      if (resParticipacao['status'] == 200) {
        this.Aluno.Participou = true;
        this.Aluno.Participacoes++;
        this.alunosService.updateAlunos(this.Aluno);
        this.toastr.success('Participação cadastrada com sucesso!');
        setTimeout(() => {
          this.alunoChosen = false;
          this.Verificado = false;
          this.btnText = "Verificar";
          this.ngOnInit();
        }, 1000);
      }
      else {
        this.toastr.error('Falha ao cadastrar participação da prova!');
      }
    })
  }

  Back() {
    if (this.alunoChosen) {
      this.alunoChosen = false;
      this.Verificado = false;
      this.btnText = "Verificar";
      this.ngOnInit();
    }
    else {
      this.route.navigate(['jogos']);
    }
  }

}