import { Component, OnInit } from '@angular/core';
import { debug } from 'util';
import { JogosComponent } from '../jogos.component';
import { Participacao } from 'src/app/models/participacaoModel';
import { Router } from '@angular/router';
import { ParticipacaoService } from 'src/app/services/participacaoService';
import { Toast, ToastrService } from 'ngx-toastr';
import { Alunos } from 'src/app/models/alunosModel';
import { AlunoService } from 'src/app/services/alunoService';
//import {NgxMaskModule} from 'ngx-mask'

@Component({
  selector: 'app-futebol',
  templateUrl: './futebol.component.html',
  styleUrls: ['./futebol.component.scss']
})
export class FutebolComponent implements OnInit {

  nbr1 = 0; nbr2 = 0;

  participacao = new Participacao();

  alunoChosen = false;

  Lista: Alunos[];
  Alunos: Alunos[];
  btnText: string = "Verificar";
  Verificado: Boolean = false;

  aluno1: number;
  aluno2: number;
  aluno3: number;
  aluno4: number;

  index: number = 0;

  winner;
  time: string = "03:00:00";
  DesabilitaBotao: Boolean = false;

  Mensagem: string = 'Número do participante já participou de outra prova!';

  constructor(private router: Router,
    private participacaoService: ParticipacaoService,
    private toastr: ToastrService,
    private alunosService: AlunoService,
    ) { }

  ngOnInit() {
    this.DesabilitaBotao = false;
    this.participacao.idProvas = 2;
    this.aluno1 = null;
    this.aluno2 = null;
    this.aluno3 = null;
    this.aluno4 = null;
    this.time = "03:00:00";
    this.winner = null;
    this.nbr1 = 0;
    this.nbr2 = 0;

    this.alunosService.getAllAlunos().then(resLista => {
      this.Lista = resLista;
    })
  }

  GoBack(){
    this.router.navigateByUrl("jogos");
  }

  ChangeNbr1(addNbr){
    var nbrnext = this.nbr1 + addNbr;
    if (nbrnext <= 3 && nbrnext >= 0){
      this.nbr1 = this.nbr1 + addNbr
    }
  }

  ChangeNbr2(addNbr){
    var nbrnext = this.nbr2 + addNbr;
    if (nbrnext <= 3 && nbrnext >= 0){
      this.nbr2 = this.nbr2 + addNbr
    }
  }

  alunosEscolhidos(){

    this.alunoChosen = true;
  }

  End() {
    this.DesabilitaBotao = true;
    this.participacao.idAluno1 = this.aluno1;
    this.participacao.idAluno2 = this.aluno2;
    this.participacao.idProvas = 2;
    this.participacao.Tempo = this.time;
    if  (this.winner == "red"){
      this.participacao.P1 = this.nbr1;
    }else {this.participacao.P1 = this.nbr2;}
    this.participacao.P2 = 0;
    this.participacao.P3 = 0;

    this.participacaoService.insertParticipacao(this.participacao).then(resParticipacao => {
      if (resParticipacao['status'] == 200) {
        this.End2();
      }
      else {
        this.toastr.error('Falha ao cadastrar participação da prova!');
      }
    })
  }

  End2(){
    this.participacao.idAluno1 = this.aluno3;
    this.participacao.idAluno2 = this.aluno4;
    this.participacao.idProvas = 2;
    this.participacao.Tempo = this.time;
    this.participacao.P1 = 0;
    this.participacao.P2 = 0;
    this.participacao.P3 = 0;

    this.participacaoService.insertParticipacao(this.participacao).then(resParticipacao => {
      if (resParticipacao['status'] == 200) {
        this.ChangeParticipou(0);
        this.toastr.success('Participação cadastrada com sucesso!');
        setTimeout(() => {
          this.alunoChosen = false;
          this.Verificado = false;
          this.btnText = "Verificar";
          this.ngOnInit();
        }, 1000);
      }
      else {
        this.toastr.error('Falha ao cadastrar participação da prova!');
      }
    })
  }

  ConfereAluno() {
    if (this.btnText == "Verificar") {
      this.Alunos = [];
      let Id1Ok = false;
      let Id2Ok = false;
      let Id3Ok = false;
      let Id4Ok = false;

      for (let i = 0; i < this.Lista.length; i++) {
        if (this.aluno1 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id1Ok = true;
        }
      }
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.aluno2 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id2Ok = true;
        }
      }
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.aluno3 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id3Ok = true;
        }
      }
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.aluno4 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id4Ok = true;
        }
      }

      setTimeout(() => {
        if (Id1Ok == true && Id2Ok == true && Id3Ok == true && Id4Ok == true) {
          this.Verificado = true;
          this.index = 0;
          this.LoopVerificacao(true);
        }
        else {
          this.toastr.error('Número do participante inválido ou já participou de outra prova!');
        }
      }, 100);
    }
    else {
      this.alunoChosen = true;
    }
  }

  LoopVerificacao(OK) {
    if (this.index < 4) {
      this.participacaoService.getParticipacoesByAluno(this.Alunos[this.index].idAlunos).then(resQtde => {
        this.Alunos[this.index].Participacoes = resQtde.length;
        if (resQtde.length == 2) {
          OK = false;
          this.index = 4;
        }
        else {
          for (let i = 0; i < resQtde.length; i++) {
            if (resQtde[i]['idProvas'] == this.participacao.idProvas) {
              OK = false;
              this.Alunos[this.index].Participacoes = 2;
              this.Mensagem = 'Participante já participou dessa prova!';
            }
          }
          this.index++;
        }
        this.LoopVerificacao(OK);
      });
    }
    else {
      if (OK)
        this.btnText = "Começar Prova";
      else
        this.toastr.error(this.Mensagem);
    }
  }

  ChangeParticipou(index: number) {
    if (index < this.Alunos.length) {
      this.Alunos[index].Participou = true;
      this.alunosService.updateAlunos(this.Alunos[index]).then(resUpdate => {
        index++;
        this.ChangeParticipou(index);
      });
    }
  }

  Back() {
    if (this.alunoChosen) {
      this.alunoChosen = false;
      this.Verificado = false;
      this.btnText = "Verificar";
      this.ngOnInit();
    }
    else {
      this.router.navigate(['jogos']);
    }
  }

}
