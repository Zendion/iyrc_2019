import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Participacao } from 'src/app/models/participacaoModel';
import { ParticipacaoService } from 'src/app/services/participacaoService';
import { ToastrService } from 'ngx-toastr';
import { Alunos } from 'src/app/models/alunosModel';
import { AlunoService } from 'src/app/services/alunoService';

@Component({
  selector: 'app-humanoide',
  templateUrl: './humanoide.component.html',
  styleUrls: ['./humanoide.component.scss']
})
export class HumanoideComponent implements OnInit {

  participacao = new Participacao();

  nbr1 = 0;

  alunoChosen = false;

  aluno1: number;

  time: string = "03:00:00";

  Lista: Alunos[];
  Aluno: Alunos;
  btnText: string = "Verificar";
  Verificado: Boolean = false;
  DesabilitaBotao: Boolean = false;

  constructor(private router: Router,
    private participacaoService: ParticipacaoService,
    private toastr: ToastrService,
    private alunosService: AlunoService,
    ) { }

  ngOnInit() {
    this.DesabilitaBotao = false;
    this.nbr1 = 0;
    this.aluno1 = null;
    this.time = "03:00:00";
    this.participacao.idProvas = 3;
    this.alunosService.getAllAlunos().then(resLista => {
      this.Lista = resLista;
    })
  }

  ChangeNbr1(addNbr){
    this.nbr1 = this.nbr1 + addNbr;
  }

  GoBack(){
    this.router.navigateByUrl("jogos");
  }

  ConfereAluno() {
    if (this.btnText == "Verificar") {
      let Encontrado: boolean = false;
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.aluno1 == this.Lista[i].idAlunos) {
          Encontrado = true;
          this.Verificado = true;
          this.Aluno = this.Lista[i];
          this.participacaoService.getParticipacoesByAluno(this.aluno1).then(resQtde => {
            if (resQtde.length < 2) {
              for (let i = 0; i < resQtde.length; i++) {
                if (resQtde[i]['idProvas'] == this.participacao.idProvas) {
                  this.toastr.error('Participante já participou dessa prova!');
                  return;
                }
              }
              this.Aluno.Participou = false;
              this.btnText = "Começar Prova";
            }
            else {
              this.Aluno.Participou = true;
              this.toastr.error('Número do participante inválido ou já participou de outra prova!');
            }
          });
        }
      }
      if (!Encontrado) {
        this.toastr.error('Número do participante não encontrado!');
        this.Verificado = false;
      }
    }
    else {
      this.alunoChosen = true;
    }
  }

  alunosEscolhidos(){
    this.alunoChosen = true;
  }

  End(){
    this.DesabilitaBotao = true;
    this.participacao.idAluno1 = this.aluno1;
    this.participacao.idAluno2 = null;
    this.participacao.idProvas = 3;
    this.participacao.Tempo = this.time;
    this.participacao.P1 = this.nbr1;
    this.participacao.P2 = 0;
    this.participacao.P3 = 0;

    this.participacaoService.insertParticipacao(this.participacao).then(resParticipacao => {
      if (resParticipacao['status'] == 200) {
        this.Aluno.Participou = true;
        this.alunosService.updateAlunos(this.Aluno);
        this.toastr.success('Participação cadastrada com sucesso!');
        setTimeout(() => {
          this.alunoChosen = false;
          this.Verificado = false;
          this.btnText = "Verificar";
          this.ngOnInit()
        }, 1000);
      }
      else {
        this.toastr.error('Falha ao cadastrar participação da prova!');
      }
    })
  }

  Back() {
    if (this.alunoChosen) {
      this.alunoChosen = false;
      this.btnText = "Verificar";
      this.Verificado = false;
      this.ngOnInit();
    }
    else {
      this.router.navigate(['jogos']);
    }
  }

}
