import { Component, OnInit, DebugElement } from '@angular/core';
import { debug } from 'util';
import { Router } from '@angular/router';
import { Participacao } from 'src/app/models/participacaoModel';

@Component({
  selector: 'app-jogos',
  templateUrl: './jogos.component.html',
  styleUrls: ['./jogos.component.scss']
})
export class JogosComponent implements OnInit {

  participacao = new Participacao();

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    
  }

  changeTo(toWhere){
    this.router.navigateByUrl('/jogos/' + toWhere);
  }

  Back() {
    this.router.navigate(['mainmenu']);
  }

}
