import { Component, OnInit } from '@angular/core';
import { Participacao } from 'src/app/models/participacaoModel';
import { ParticipacaoService } from 'src/app/services/participacaoService';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Alunos } from 'src/app/models/alunosModel';
import { AlunoService } from 'src/app/services/alunoService';

@Component({
  selector: 'app-volei',
  templateUrl: './volei.component.html',
  styleUrls: ['./volei.component.scss']
})
export class VoleiComponent implements OnInit {

  nbr1: number = 0;
  alunoChosen: Boolean = false;
  Participacao1: Participacao;
  Participacao2: Participacao;
  T1Win: Boolean;
  TempoPartida: string = "03:00:00";
  Lista: Alunos[];
  Alunos: Alunos[]
  btnText: string = "Verificar";
  Verificado: Boolean = false;
  index: number = 0;
  MaxBolinhas: number = 30;
  DesabilitaBotao: Boolean = false;

  Mensagem: string = 'Número do participante já participou de outra prova!';

  constructor(
    private participacaoService: ParticipacaoService,
    private toastr: ToastrService,
    private route: Router,
    private alunosService: AlunoService,
  ) { }

  ngOnInit() {
    this.DesabilitaBotao = false;
    this.Participacao1 = new Participacao();
    this.Participacao1.idProvas = 5;
    this.Participacao1.P1 = 0;
    this.Participacao1.P2 = 0;
    this.Participacao1.P3 = 0;
    this.Participacao1.Tempo = "03:00:00";

    this.Participacao2 = new Participacao();
    this.Participacao2.idProvas = 5;
    this.Participacao2.P1 = 0;
    this.Participacao2.P2 = 0;
    this.Participacao2.P3 = 0;
    this.Participacao2.Tempo = "03:00:00";

    this.alunosService.getAllAlunos().then(resLista => {
      this.Lista = resLista;
    })
  }

  ChangeNbr(addNbr) {
    if (this.nbr1 + addNbr >= 0 && this.nbr1 + addNbr <= 100)
      this.nbr1 += addNbr;
  }

  ConfereAluno() {
    if (this.btnText == "Verificar") {
      this.Alunos = [];
      let Id1Ok = false;
      let Id2Ok = false;
      let Id3Ok = false;
      let Id4Ok = false;

      for (let i = 0; i < this.Lista.length; i++) {
        if (this.Participacao1.idAluno1 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id1Ok = true;
        }
      }
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.Participacao1.idAluno2 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id2Ok = true;
        }
      }
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.Participacao2.idAluno1 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id3Ok = true;
        }
      }
      for (let i = 0; i < this.Lista.length; i++) {
        if (this.Participacao2.idAluno2 == this.Lista[i].idAlunos) {
          this.Alunos.push(this.Lista[i]);
          Id4Ok = true;
        }
      }

      setTimeout(() => {
        if (Id1Ok == true && Id2Ok == true && Id3Ok == true && Id4Ok == true) {
          this.Verificado = true;
          this.index = 0;
          this.LoopVerificacao(true);
        }
        else {
          this.toastr.error('Número do participante inválido ou já participou de outra prova!');
        }
      }, 100);
    }
    else {
      this.alunoChosen = true;
    }
  }

  LoopVerificacao(OK) {
    if (this.index < 4) {
      this.participacaoService.getParticipacoesByAluno(this.Alunos[this.index].idAlunos).then(resQtde => {
        this.Alunos[this.index].Participacoes = resQtde.length;
        if (resQtde.length == 2) {
          OK = false;
          this.index = 4;
        }
        else {
          for (let i = 0; i < resQtde.length; i++) {
            if (resQtde[i]['idProvas'] == this.Participacao1.idProvas) {
              OK = false;
              this.Alunos[this.index].Participacoes = 2;
              this.Mensagem = 'Participante já participou dessa prova!';
            }
          }
          this.index++;
        }
        this.LoopVerificacao(OK);
      });
    }
    else {
      if (OK)
        this.btnText = "Começar Prova";
      else
        this.toastr.error(this.Mensagem);
    }
  }

  End() {
    this.DesabilitaBotao = true;
    if (this.T1Win) {
      this.Participacao1.P1 = this.MaxBolinhas - this.nbr1;
      this.Participacao1.Tempo = this.TempoPartida;
    }
    else {
      this.Participacao2.P1 = this.MaxBolinhas - this.nbr1;
      this.Participacao2.Tempo = this.TempoPartida;
    }

    this.participacaoService.insertParticipacao(this.Participacao1).then(resParticipacao => {
      this.participacaoService.insertParticipacao(this.Participacao2).then(resParticipacao => {
        if (resParticipacao['status'] == 200) {
          this.ChangeParticipou(0);
          this.toastr.success('Participação cadastrada com sucesso!');
          setTimeout(() => {
            this.btnText = "Verificar";
            this.Verificado = false;
            this.alunoChosen = false;
            this.ngOnInit();
          }, 1000);
        }
        else {
          this.toastr.error('Falha ao cadastrar participação da prova!');
        }
      })  
    })
  }

  ChangeParticipou(index: number) {
    if (index < this.Alunos.length) {
      this.Alunos[index].Participou = true;
      this.alunosService.updateAlunos(this.Alunos[index]).then(resUpdate => {
        index++;
        this.ChangeParticipou(index);
      });
    }
  }

  Back() {
    if (this.alunoChosen) {
      this.alunoChosen = false;
      this.Verificado = false;
      this.btnText = "Verificar";
      this.ngOnInit();
    }
    else {
      this.route.navigate(['jogos']);
    }
  }

}
