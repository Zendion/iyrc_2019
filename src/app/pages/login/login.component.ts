import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router'

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  clickMessage = "";

  constructor(private toastr: ToastrService,
              private router: Router) { }

  ngOnInit() {
  }

  showSuccess()
  {
    this.toastr.success('Login efetuado com sucesso');
  }

  showFail()
  {
    this.toastr.error('Usuário/Senha incorretos, por favor cheque as informações inseridas');
  }

  onSubmit(f: NgForm)
  {
    console.debug("Deveria funcionar");
    if (f.value.login === "admin" && f.value.password === "maker2019")
    {
      this.clickMessage = "Login Correto";
      this.showSuccess();
      setTimeout(() => {
        this.router.navigate(['/mainmenu']);
      }, 3000);
      
    }
    else
    {
      this.clickMessage = "Login Incorreto";
      this.showFail();
    }
  }

}
