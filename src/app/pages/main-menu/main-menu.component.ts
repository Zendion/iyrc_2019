import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  redirectTo(n: number)
  {
    switch(n)
    {
      case 0 :
      this.router.navigate(['jogos']);
      break;

      case 1:
        this.router.navigate(['cadastro/alunos']);
        break;
      
      case 2:
        this.router.navigate(['cadastro/escolas']);
        break;
        
      case 3:
        this.router.navigate(['placar']);
        break;
    }
  }

}
