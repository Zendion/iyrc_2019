import { Component, OnInit, Injectable, Input } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AlunoPlacar } from 'src/app/models/placarModel';

@Component({
  selector: 'app-competidor',
  templateUrl: './competidor.component.html',
  styleUrls: ['./competidor.component.scss'],
  animations: [
    trigger('openClose', [ 
      state('open', style({
        opacity: 1,
        transform: 'translateY(0)'
      })),
      state('closed',style({
        opacity:0,
        transform: '  translateY(-100%)'
      })),
      transition('open <=> closed', [
        animate('.5s')
      ]),
    ])
  ]
})
export class CompetidorComponent implements OnInit {
  @Input()
  openTimer = 1000;

  @Input()
  compAluno = new AlunoPlacar();

  displayThis = false;

  constructor() { }
  

  ngOnInit() {
    console.log(this.compAluno);
    setTimeout(() => {
      this.displayThis = true;
    }, this.openTimer);
  }

}
