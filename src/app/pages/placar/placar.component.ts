import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AlunoPlacar } from 'src/app/models/placarModel';
import { ParticipacaoService } from 'src/app/services/participacaoService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-placar',
  templateUrl: './placar.component.html',
  styleUrls: ['./placar.component.scss'],
  animations : [
    trigger('openClose', [
      state('open',style({
        opacity: 1,
        transform: 'translateY(0)'
      })),
      state('closed',style({
        opacity:0,
        transform: 'translateY(-100%)'
      })),
      transition('open <=> closed', [
        animate('1s')
      ]),
    ]),
  ],
})

export class PlacarComponent implements OnInit {
  isOpen = false;
  parts : AlunoPlacar[];
  currentIndex = 0;
  currentDelay = 500;
  alreadyChanged = false;
  timer : any;

  constructor(private placarService: ParticipacaoService,
              private router: Router) { }

  ngOnInit() {
    this.GetNext();
  }



  GetNext()
  {
    this.currentIndex++;
    if(this.currentIndex > 7)
    {
      this.currentIndex = 1;
    }
    this.placarService.getPlacarByProva(this.currentIndex).then(resPlacar => {
      this.parts = resPlacar;
    })
    setTimeout(() => {
      this.isOpen = true;
      this.alreadyChanged = false;
    }, this.currentDelay);
    this.awaitForBye();
  }

  addDelay()
  {
    this.currentDelay += 500;
    return this.currentDelay;
  }

  awaitForBye()
  {
    this.timer = setTimeout(() => {
      if (!this.alreadyChanged)
      {
       this.doByebye(true);
      }
    }, 20000);
    
  }

  doByebye(f: boolean)
  {
    clearTimeout(this.timer);
    this.isOpen = false;
    setTimeout(() => {
      if (f)
      {
        this.GetNext();
      }
      else
      {
        this.getPrevious();
      }

      this.alreadyChanged = true;
    }, 1000);
  }

  getPrevious()
  {
    this.currentIndex--;
    if(this.currentIndex > 5 || this.currentIndex <= 0)
    {
      this.currentIndex = 1;
    }
    this.placarService.getPlacarByProva(this.currentIndex).then(resPlacar => {
      this.parts = resPlacar;
    })
    setTimeout(() => {
      this.isOpen = true;
      this.alreadyChanged = false;
    }, this.currentDelay);
    this.awaitForBye();
  }

  backToMenu()
  {
    this.router.navigate(['/mainmenu']);
  }
}