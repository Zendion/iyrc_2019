import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalFunctions } from 'src/environments/environment';
import { Alunos, ListAlunos } from '../models/alunosModel';

@Injectable()
export class AlunoService {

  Rota: String = "/alunos";

  constructor(
    private http: HttpClient,
    private globalFunc: GlobalFunctions
    ) { }

  getAllAlunos() {
    return this.http.get<Alunos[]>(`${this.globalFunc.ApiUrl() + this.Rota}/getAllAlunos`)
      .toPromise()
      .then(response => <Alunos[]>response)
      .then(data => {
        return data['response'];
      })
  }

  getListAlunos() {
    return this.http.get<ListAlunos[]>(`${this.globalFunc.ApiUrl() + this.Rota}/getListAlunos`)
      .toPromise()
      .then(response => <ListAlunos[]>response)
      .then(data => {
        return data['response'];
      })
  }

  getAlunoByID(idAlunos: number) {
    return this.http.get<Alunos>(`${this.globalFunc.ApiUrl() + this.Rota}/getAlunoByID/${idAlunos}`)
      .toPromise()
      .then(response => <Alunos>response)
      .then(data => {
        return data['response'][0];
      })
  }

  insertAlunos(formAlunos: Alunos) {
    const response = this.http
      .post(`${this.globalFunc.ApiUrl() + this.Rota}/insertAlunos`, formAlunos)
      .toPromise();
    return response;
  }

  updateAlunos(formAlunos: Alunos) {
    const response = this.http
      .patch(`${this.globalFunc.ApiUrl() + this.Rota}/updateAlunos`, formAlunos)
      .toPromise();
    return response;
  }

  deleteAlunos(idAlunos: number) {
    const response = this.http
      .delete(`${this.globalFunc.ApiUrl() + this.Rota}/deleteAlunos/${idAlunos}`)
      .toPromise();
    return response;
  }

}