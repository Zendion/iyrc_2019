import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalFunctions } from 'src/environments/environment';
import { Escolas } from '../models/escolasModel';

@Injectable()
export class EscolaService {

  Rota: String = "/escolas";

  constructor(
    private http: HttpClient,
    private globalFunc: GlobalFunctions
  ) { }

  getAllEscolas() {
    return this.http.get<Escolas[]>(`${this.globalFunc.ApiUrl() + this.Rota}/getAllEscolas`)
      .toPromise()
      .then(response => <Escolas[]>response)
      .then(data => {
        return data['response'];
      })
  }

  getEscolaByID(idEscolas: number) {
    return this.http.get<Escolas>(`${this.globalFunc.ApiUrl() + this.Rota}/getEscolaByID/${idEscolas}`)
      .toPromise()
      .then(response => <Escolas>response)
      .then(data => {
        return data['response'][0];
      })
  }

  insertEscolas(formEscolas: Escolas) {
    const response = this.http
      .post(`${this.globalFunc.ApiUrl() + this.Rota}/insertEscolas`, formEscolas)
      .toPromise();
    return response;
  }

  updateEscolas(formEscolas: Escolas) {
    const response = this.http
      .patch(`${this.globalFunc.ApiUrl() + this.Rota}/updateEscolas`, formEscolas)
      .toPromise();
    return response;
  }

  deleteNoticias(idEscolas: number) {
    const response = this.http
      .delete(`${this.globalFunc.ApiUrl() + this.Rota}/deleteEscolas/${idEscolas}`)
      .toPromise();
    return response;
  }

}