import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalFunctions } from 'src/environments/environment';
import { Participacao, ParticipacaoCont } from '../models/participacaoModel';
import { AlunoPlacar } from '../models/placarModel';

@Injectable()
export class ParticipacaoService {

  Rota: String = "/participacao";

  constructor(
    private http: HttpClient,
    private globalFunc: GlobalFunctions
  ) { }

  getAllParticipacao() {
    return this.http.get<Participacao[]>(`${this.globalFunc.ApiUrl() + this.Rota}/getAllParticipacao`)
      .toPromise()
      .then(response => <Participacao[]>response)
      .then(data => {
        return data['response'];
      })
  }

  getParticipacaoByID(idParticipacao: number) {
    return this.http.get<Participacao>(`${this.globalFunc.ApiUrl() + this.Rota}/getParticipacaoByID/${idParticipacao}`)
      .toPromise()
      .then(response => <Participacao>response)
      .then(data => {
        return data['response'][0];
      })
  }

  getParticipacoesByAluno(idAlunos: number) {
    return this.http.get<ParticipacaoCont>(`${this.globalFunc.ApiUrl() + this.Rota}/getParticipacoesByAluno/${idAlunos}`)
      .toPromise()
      .then(response => <ParticipacaoCont>response)
      .then(data => {
        return data['response'];
      })
  }

  getPlacarByProva(idProvas: number) {
    return this.http.get<AlunoPlacar[]>(`${this.globalFunc.ApiUrl() + this.Rota}/getPlacar/${idProvas}`)
      .toPromise()
      .then(response => <AlunoPlacar[]>response)
      .then(data => {
        return data['response'];
      })
  }

  insertParticipacao(formParticipacao: Participacao) {
    const response = this.http
      .post(`${this.globalFunc.ApiUrl() + this.Rota}/insertParticipacao`, formParticipacao)
      .toPromise();
    return response;
  }

  updateParticipacao(formParticipacao: Participacao) {
    const response = this.http
      .patch(`${this.globalFunc.ApiUrl() + this.Rota}/updateParticipacao`, formParticipacao)
      .toPromise();
    return response;
  }

  deleteParticipacao(idParticipacao: number) {
    const response = this.http
      .delete(`${this.globalFunc.ApiUrl() + this.Rota}/deleteParticipacao/${idParticipacao}`)
      .toPromise();
    return response;
  }

}