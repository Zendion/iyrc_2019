import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalFunctions } from 'src/environments/environment';
import { Provas } from '../models/provasModel';

@Injectable()
export class ProvasService {

  Rota: String = "/provas";

  constructor(
    private http: HttpClient,
    private globalFunc: GlobalFunctions
  ) { }

  getAllProvas() {
    return this.http.get<Provas[]>(`${this.globalFunc.ApiUrl() + this.Rota}/getAllProvas`)
      .toPromise()
      .then(response => <Provas[]>response)
      .then(data => {
        return data['response'];
      })
  }

  getProvaByID(idProvas: number) {
    return this.http.get<Provas>(`${this.globalFunc.ApiUrl() + this.Rota}/getProvaByID/${idProvas}`)
      .toPromise()
      .then(response => <Provas>response)
      .then(data => {
        return data['response'][0];
      })
  }

  insertProva(formProvas: Provas) {
    const response = this.http
      .post(`${this.globalFunc.ApiUrl() + this.Rota}/insertProvas`, formProvas)
      .toPromise();
    return response;
  }

  updateProvas(formProvas: Provas) {
    const response = this.http
      .patch(`${this.globalFunc.ApiUrl() + this.Rota}/updateProvas`, formProvas)
      .toPromise();
    return response;
  }

  deleteProvas(idProvas: number) {
    const response = this.http
      .delete(`${this.globalFunc.ApiUrl() + this.Rota}/deleteProvas/${idProvas}`)
      .toPromise();
    return response;
  }

}