import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalFunctions } from 'src/environments/environment';
import { Series } from '../models/seriesModel';

@Injectable()
export class SerieService {

  Rota: String = "/series";

  constructor(
    private http: HttpClient,
    private globalFunc: GlobalFunctions
    ) { }

  getAllSeries() {
    return this.http.get<Series[]>(`${this.globalFunc.ApiUrl() + this.Rota}/getAllSeries`)
      .toPromise()
      .then(response => <Series[]>response)
      .then(data => {
        return data['response'];
      })
  }

  getSeriesByID(idSeries: number) {
    return this.http.get<Series>(`${this.globalFunc.ApiUrl() + this.Rota}/getAlunoByID/${idSeries}`)
      .toPromise()
      .then(response => <Series>response)
      .then(data => {
        return data['response'][0];
      })
  }

  insertSeries(formSeries: Series) {
    const response = this.http
      .post(`${this.globalFunc.ApiUrl() + this.Rota}/insertSeries`, formSeries)
      .toPromise();
    return response;
  }

  updateSeries(formSeries: Series) {
    const response = this.http
      .patch(`${this.globalFunc.ApiUrl() + this.Rota}/updateSeries`, formSeries)
      .toPromise();
    return response;
  }

  deleteSeries(idSeries: number) {
    const response = this.http
      .delete(`${this.globalFunc.ApiUrl() + this.Rota}/deleteSeries/${idSeries}`)
      .toPromise();
    return response;
  }

}