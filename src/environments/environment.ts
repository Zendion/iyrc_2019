export const environment = {
  production: false
};

export class GlobalFunctions {
  ApiUrl() {
    return "http://" + localStorage.getItem("IP") + ":1337";
  }
}